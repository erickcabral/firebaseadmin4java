/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.maven.firebaseadmin4java;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.annotations.NotNull;
import com.firebase.security.token.TokenGenerator;
import com.firebase.security.token.TokenOptions;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mylibs.maven.easyjson.EasyJson;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erick Cabral
 */
public class FirebaseAdmin4Java {

	Logger LOG_FIREBASE_ADMIN = Logger.getLogger(FirebaseAdmin4Java.class.getName());

	ExecutorService executor;

	String servicePath; // = "F:\\Cursos\\JAVA SE\\Bibliotecas\\NovoHyper\\hyperSecret.json";
	String key; // = "bNbND1r0aPe2T7MYbdmr9tnayoLPLsD1bO8LAt4q";
	String projectUrl; // = "https://hypertrophy-fire.firebaseio.com/";

	Firebase firebase;

	FileInputStream serviceStream = null;
	GoogleCredentials credential = null;

	FirebaseApp fireApp = null;
	FirebaseOptions options = null;
	FirestoreOptions firestoreOptions = null;

	private FirebaseAuth firebaseAuth;
	private UserRecord.CreateRequest userCreator;
	private UserInfo firebaseUser;

	private boolean task_completed;

	FirebaseDatabase firebaseDatabase;

	HashMap<String, String> dataToInput = new HashMap<>();

	TokenGenerator fireTokenGenerator;
	TokenOptions fireTokenOptions = new TokenOptions();

	Firestore firestoreDb = null;

	private boolean userOnline = false;

	private DataSnapshot objectToRetrieve = null;

	EasyJson eassyJson = new EasyJson();

	public boolean isUserOnline() {
		return userOnline;
	}

	public FirebaseAdmin4Java(String servicePath, String key, String projectUrl) {
		this.servicePath = servicePath;
		this.key = key;
		this.projectUrl = projectUrl;
	}

	public void startFirebase() throws FileNotFoundException, IOException {
		LOG_FIREBASE_ADMIN.info(">>> Setting Up Firebase <<<");
		this.serviceStream = new FileInputStream(this.servicePath);
		this.credential = GoogleCredentials.fromStream(this.serviceStream);
		//System.out.println("CRED -> " + credential);
		this.options = FirebaseOptions.builder()
			.setCredentials(this.credential)
			.setDatabaseUrl(this.projectUrl)
			.build();

		this.firestoreOptions = FirestoreOptions.newBuilder()
			.setCredentials(credential)
			.setTimestampsInSnapshotsEnabled(true).build();

		this.fireApp = FirebaseApp.initializeApp(this.options);

		this.firebase = new Firebase(this.projectUrl);
		this.firebaseAuth = FirebaseAuth.getInstance(this.fireApp);
		this.firebaseDatabase = FirebaseDatabase.getInstance(this.fireApp);
	}

	public FirebaseDatabase getFirebaseDatabase() {
		return this.firebaseDatabase;
	}

	public void setDatabaseFolder(DatabaseReference databaseReference, String child_folder) {
		databaseReference = this.firebaseDatabase.getReference().child(child_folder);
	}

	public Future<String> registerUserAsync(String username, String email, String password) {
		userCreator = new UserRecord.CreateRequest();
		userCreator.setDisplayName(username);
		userCreator.setEmail(email);
		userCreator.setPassword(password);
		//userInfo.setPhoneNumber(userProperties.getProperty("phone"));
		//userInfo.setPhotoUrl(userProperties.getProperty("photoUrl"));
		userCreator.setEmailVerified(false);
		userCreator.setDisabled(false);

		this.executor = Executors.newSingleThreadExecutor();
		return this.executor.submit(new Callable<String>() {
			@Override
			public String call() throws Exception {
				String UID = firebaseAuth.createUser(userCreator).getUid();
				while (UID == null) {
					TimeUnit.MILLISECONDS.sleep(1000);
					LOG_FIREBASE_ADMIN.warning(">>> REGISTERING RUNNING...");
				}
				LOG_FIREBASE_ADMIN.log(Level.INFO, ">>> NEW USER UID {0}", UID);
				executor.shutdown();
				return UID;
			}
		});
	}

	public Future<UserInfo> loginUser(@NotNull String email, @NotNull String pass, boolean admin) throws InterruptedException {
		this.executor = Executors.newSingleThreadExecutor();

		/*
			Verificar PASSWORD
			if(password Correto){
				prosseguir.
			}
		
		if (email.isEmpty() || pass.isEmpty()) {
			throw new NullPointerException(">>> EMAIL/PASSWORD MISSING <<<");
		}
		 */
		UserInfo fireUSer;
		HashMap<String, Object> userData = new HashMap<>();

		try {
			fireUSer = firebaseAuth.getUserByEmail(email);
			userData.put("uid", fireUSer.getUid());
			userData.put("email", email);
			userData.put("password", pass);
		}
		catch (FirebaseAuthException ex) {
			Logger.getLogger(FirebaseAdmin4Java.class.getName()).log(Level.SEVERE, null, ex);
			throw new NullPointerException(">>>> SIGNING EXCEPTION >>> User Not Found");
		}

		this.fireTokenOptions.setAdmin(admin); // TODO: <-- REVIEW if it's Necessary

		TokenGenerator tkGen = new TokenGenerator(this.key);
		String token = tkGen.createToken(userData, fireTokenOptions);

		this.firebaseUser = null;
		this.task_completed = false;
		this.firebase.authWithCustomToken(token, new Firebase.AuthResultHandler() {
			@Override
			public void onAuthenticated(AuthData ad) {
				try {
					firebaseUser = firebaseAuth.getUserAsync(ad.getUid()).get();
				}
				catch (InterruptedException | ExecutionException ex) {
					Logger.getLogger(FirebaseAdmin4Java.class.getName()).log(Level.SEVERE, null, ex);
				}
				LOG_FIREBASE_ADMIN.log(Level.SEVERE, ">>> USER LOGGED SUCCESSFULLY >>> {0}", ad.getUid());
				task_completed = true;
			}

			@Override
			public void onAuthenticationError(FirebaseError fe) {
				LOG_FIREBASE_ADMIN.log(Level.SEVERE, ">>> ERROR LOGGING >>> {0}", fe.getMessage());
				task_completed = true;
			}
		});

		return this.executor.submit(new Callable<UserInfo>() {
			@Override
			public UserInfo call() throws Exception {
				while (!task_completed) {
					TimeUnit.MILLISECONDS.sleep(1000);
					LOG_FIREBASE_ADMIN.warning(">>> FIREBASE SIGNING RUNNING... <<<");
				}
				executor.shutdown();
				LOG_FIREBASE_ADMIN.warning(">>> SIGNING EXECUTOR FINISHED <<<");
				return firebaseUser;
			}
		});
	}

	public void writeAsyncData(DatabaseReference reference, Object object, long timeout) throws InterruptedException, ExecutionException, TimeoutException {
		reference.setValueAsync(object).get(timeout, TimeUnit.MILLISECONDS);
	}

	//TODO RETRIEVE DATA FROM FIREBASE REAL TIME DATABASE
	public Future<DataSnapshot> retrieveDataSnapshot(DatabaseReference databaseReference) throws InterruptedException {
		this.executor = Executors.newCachedThreadPool();
		return this.executor.submit(new Callable<DataSnapshot>() {
			@Override
			public DataSnapshot call() throws Exception {
				task_completed = false;
				databaseReference.addListenerForSingleValueEvent(valueEventListener); //VAI RETORNAR O OBJETO...
				while (!task_completed) {
					LOG_FIREBASE_ADMIN.warning(">>> Retrieving data .... <<<");
					TimeUnit.MILLISECONDS.sleep(1000);
				}
				databaseReference.removeEventListener(valueEventListener);
				executor.shutdown();
				LOG_FIREBASE_ADMIN.log(Level.INFO, ">>> RETRIEVED OBJECT NULL? {0} <<<", (objectToRetrieve == null));
				return objectToRetrieve;
			}
		});
	}

	public boolean deleteUser(String userUID, long timeout) {
		try {
			this.firebaseAuth.deleteUserAsync(userUID).get(timeout, TimeUnit.MILLISECONDS);
			return true;
		}
		catch (InterruptedException | ExecutionException | TimeoutException ex) {
			Logger.getLogger(FirebaseAdmin4Java.class.getName()).log(Level.SEVERE, ">>> ERROR DELETING USER <<<", ex);
			return false;
		}
	}

	public void closeConnection() {
		this.firebase.getApp().goOffline();
	}

	private ValueEventListener valueEventListener = new ValueEventListener() {
		@Override
		public void onDataChange(DataSnapshot snapshot) {
			System.out.println("RETRIEVING OBEJECT FOUND - > " + snapshot.getKey());
			System.out.println("FOUND - > " + snapshot.getValue());
			objectToRetrieve = snapshot;
			task_completed = true;
		}

		@Override
		public void onCancelled(DatabaseError error) {
			System.out.println("RETRIEVING ERROR -> " + error);
			task_completed = true;
		}
	};
}
