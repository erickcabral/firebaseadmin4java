/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.erickcabral.maven.firebaseadmin4java.FirebaseAdmin4Java;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Erick Cabral
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InitTest {

	static private FirebaseAdmin4Java FIREBASE_ADMIN;
	static private final String secretPath = "F:\\Cursos\\JAVA SE\\Bibliotecas\\NovoHyper\\hyperSecret.json";
	static private final String secretKey = "bNbND1r0aPe2T7MYbdmr9tnayoLPLsD1bO8LAt4q";
	static private final String projectURL = "https://hypertrophy-fire.firebaseio.com/";

	String userName = "fofis";
	static String USER_UID = null;
	String userMail = "fofis@hyper.com";
	String name = "Fofilvlissis";
	String surname = "da Silva";
	String password = "321321";

	Properties measuresValues;
	String TESTING_VALUE = "62.5";

	private static final String STRING_FIREBASE_USER_FOLDER = "_USERS_";
	private final String STRING_FIREBASE_PROFILE_FOLDER = "_PROFILE_";
	private final String STRING_FIREBASE_MEASURES_FOLDER = "_MEASURES_";
	private final String STRING_FIREBASE_INITIAL_FOLDER = "_INITIAL_";
	private final String STRING_FIREBASE_UPDATES_FOLDER = "_UPDATES_";

	private static DatabaseReference HYPER_USER_FOLDER;

	private static DatabaseReference USER_UID_FOLDER;
	private static DatabaseReference USER_PROFILE_FOLDER;
	private static DatabaseReference USER_INITIAL_MEASURES_FOLDER;

	public InitTest() {

	}

	@BeforeClass
	public static void setUpClass() {
		FIREBASE_ADMIN = new FirebaseAdmin4Java(secretPath, secretKey, projectURL);
		try {
			FIREBASE_ADMIN.startFirebase();
			HYPER_USER_FOLDER = FIREBASE_ADMIN.getFirebaseDatabase().getReference().child(STRING_FIREBASE_USER_FOLDER);

		}
		catch (IOException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Before
	public void setUp() {

		measuresValues = new Properties();

		measuresValues.setProperty("DATE", Calendar.getInstance().getTime().toString());
		measuresValues.setProperty("HEIGHT", TESTING_VALUE);
		measuresValues.setProperty("WEIGHT", TESTING_VALUE);
		measuresValues.setProperty("FAT", TESTING_VALUE);
		measuresValues.setProperty("SHOULDERS", TESTING_VALUE);
		measuresValues.setProperty("CHEST", TESTING_VALUE);

		measuresValues.setProperty("WAIST", TESTING_VALUE);
		measuresValues.setProperty("LEFT_ARM", TESTING_VALUE);
		measuresValues.setProperty("RIGHT_ARM", TESTING_VALUE);
		measuresValues.setProperty("GLUTES", TESTING_VALUE);
		measuresValues.setProperty("LEFT_THIGH", TESTING_VALUE);
		measuresValues.setProperty("RIGHT_THIGH", TESTING_VALUE);
		measuresValues.setProperty("LEFT_CALF", TESTING_VALUE);
		measuresValues.setProperty("RIGHT_CALF", TESTING_VALUE);
	}

	//@Test
	public void test_1_resgisterUser_notNull() {
		try {
			this.USER_UID = FIREBASE_ADMIN.registerUserAsync(userName, userMail, password).get();
			System.out.println("GET >>> " + this.USER_UID);
		}
		catch (InterruptedException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch (ExecutionException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println("RESULT >>> " + this.USER_UID);
		assertNotNull(this.USER_UID);

	}

	@Test
	public void teste_2_loginUser_notNull() {
		try {
			UserInfo fireUser = FIREBASE_ADMIN.loginUser(userMail, password, true).get();
			USER_UID = fireUser.getUid();
		}
		catch (InterruptedException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch (ExecutionException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println("GET >>> " + USER_UID);
		System.out.println(">>> AUTH >>> " + USER_UID);

		assertNotNull(USER_UID);
	}

	@Test
	public void teste_3_uploadDataAsync() {
		System.out.println("USER >>> " + this.USER_UID);
		if (USER_UID != null) {
			Person user = new Person(name, surname, userMail);
			USER_UID_FOLDER = HYPER_USER_FOLDER.child(USER_UID);
			System.out.println(">>>>>>>>>>. " + USER_UID_FOLDER);
			
			USER_PROFILE_FOLDER = USER_UID_FOLDER.child(this.STRING_FIREBASE_PROFILE_FOLDER);
			System.out.println(">>>>>>>>>>. " + USER_PROFILE_FOLDER);
			try {
				FIREBASE_ADMIN.writeAsyncData(USER_PROFILE_FOLDER, user, 5000);
			}
			catch (InterruptedException | ExecutionException | TimeoutException ex) {
				Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
			}
			
			USER_INITIAL_MEASURES_FOLDER = USER_UID_FOLDER.child(STRING_FIREBASE_MEASURES_FOLDER).child(STRING_FIREBASE_INITIAL_FOLDER);
			System.out.println(">>>>>>>>>>. " + USER_INITIAL_MEASURES_FOLDER);
			try {
				FIREBASE_ADMIN.writeAsyncData(USER_INITIAL_MEASURES_FOLDER, this.measuresValues, 1000);
			}
			catch (InterruptedException | ExecutionException | TimeoutException ex) {
				Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
	
	@Test
	public void teste_4_retrieveData_1(){
		DataSnapshot ret = null;
		try {
			ret =  FIREBASE_ADMIN.retrieveDataSnapshot(USER_PROFILE_FOLDER).get();
		}
		catch (InterruptedException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch (ExecutionException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println(">>> SNAPT <<< " + ret);
		assertNotNull(ret);
	}
	
	@Test
	public void teste_5_retrieveData_2(){
		DataSnapshot ret = null;
		try {
			ret =  FIREBASE_ADMIN.retrieveDataSnapshot(USER_INITIAL_MEASURES_FOLDER).get();
		}
		catch (InterruptedException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		catch (ExecutionException ex) {
			Logger.getLogger(InitTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		System.out.println(">>> SNAPT <<< " + ret);
		assertNotNull(ret);
	}

}

class Person {

	String name, surname, email;

	public Person(String name, String surname, String email) {
		this.name = name;
		this.surname = surname;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
